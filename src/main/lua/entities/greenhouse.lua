local greenhouse = {
	type = "assembling-machine",
	name = "greenhouse",
	icons = {
		{
			icon = "__base__/graphics/icons/lab.png",
			tint = { r = 0.34, g = 0.72, b = 0.28, a = 0.6 }
		}
	},
	icon_size = 32,
	flags = {"placeable-neutral", "placeable-player", "player-creation"},
	minable = {hardness = 0.2, mining_time = 0.5, result = "greenhouse"},
	max_health = 350,
	corpse = "big-remnants",
	dying_explosion = "medium-explosion",
	alert_icon_shift = util.by_pixel(-3, -12),
	resistances = {
		{
			type = "fire",
			percent = 70
		}
	},
	fluid_boxes = {
		{
			production_type = "input",
			pipe_picture = assembler2pipepictures(),
			pipe_covers = pipecoverspictures(),
			base_area = 10,
			base_level = -1,
			pipe_connections = {{ type="input", position = {0, -2} }},
			secondary_draw_orders = { north = -1 }
		},
		{
			production_type = "output",
			pipe_picture = assembler2pipepictures(),
			pipe_covers = pipecoverspictures(),
			base_area = 10,
			base_level = 1,
			pipe_connections = {{ type="output", position = {0, 2} }},
			secondary_draw_orders = { north = -1 }
		},
		off_when_no_fluid_recipe = true
	},
	collision_box = {{-1.2, -1.2}, {1.2, 1.2}},
	selection_box = {{-1.5, -1.5}, {1.5, 1.5}},
    light = {intensity = 0.75, size = 8, color = {r = 1.0, g = 1.0, b = 1.0}},
	animation = {
		layers = {
			{
				filename = "__base__/graphics/entity/lab/lab.png",
				tint = { r = 0.34, g = 0.72, b = 0.28, a = 0.6 },
				width = 98,
				height = 87,
				frame_count = 33,
				line_length = 11,
				animation_speed = 1 / 3,
				shift = util.by_pixel(0, 1.5),
				hr_version = {
					filename = "__base__/graphics/entity/lab/hr-lab.png",
					tint = { r = 0.34, g = 0.72, b = 0.28, a = 0.6 },
					width = 194,
					height = 174,
					frame_count = 33,
					line_length = 11,
					animation_speed = 1 / 3,
					shift = util.by_pixel(0, 1.5),
					scale = 0.5
				}
			},
			{
				filename = "__base__/graphics/entity/lab/lab-integration.png",
				width = 122,
				height = 81,
				frame_count = 1,
				line_length = 1,
				repeat_count = 33,
				animation_speed = 1 / 3,
				shift = util.by_pixel(0, 15.5),
				hr_version = {
					filename = "__base__/graphics/entity/lab/hr-lab-integration.png",
					width = 242,
					height = 162,
					frame_count = 1,
					line_length = 1,
					repeat_count = 33,
					animation_speed = 1 / 3,
					shift = util.by_pixel(0, 15.5),
					scale = 0.5
				}
			},
			{
				filename = "__base__/graphics/entity/lab/lab-shadow.png",
				width = 122,
				height = 68,
				frame_count = 1,
				line_length = 1,
				repeat_count = 33,
				animation_speed = 1 / 3,
				shift = util.by_pixel(13, 11),
				draw_as_shadow = true,
				hr_version = {
					filename = "__base__/graphics/entity/lab/hr-lab-shadow.png",
					width = 242,
					height = 136,
					frame_count = 1,
					line_length = 1,
					repeat_count = 33,
					animation_speed = 1 / 3,
					shift = util.by_pixel(13, 11),
					scale = 0.5,
					draw_as_shadow = true
				}
			}
		}
	},
	vehicle_impact_sound =  { filename = "__base__/sound/car-metal-impact.ogg", volume = 0.65 },
	picture = {
		filename = "__base__/graphics/entity/lab/lab.png",
		priority = "extra-high",
		width = 38,
		height = 32,
		shift = { 0.09375, 0 }
	},
	crafting_categories = {"crafting", "advanced-crafting", "crafting-with-fluid"},
	crafting_speed = 0.75,
	energy_source = {
		type = "electric",
		usage_priority = "secondary-input",
		emissions = 0.04 / 2.5
	},
	energy_usage = "150kW",
	ingredient_count = 4,
	module_specification = {
		module_slots = 3
	},
	allowed_effects = {"consumption", "speed", "productivity", "pollution"}
}

data:extend{greenhouse}
