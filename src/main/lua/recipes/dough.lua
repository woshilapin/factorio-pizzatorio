local dough_recipe = {
	type = "recipe",
	name = "dough",
	enabled = true,
	hidden = false,
	category = "crafting-with-fluid",
	ingredients = {
		{
			type = "fluid",
			name = "water",
			amount = 1
		},
		{
			type = "item",
			name = "yeast",
			amount = 1
		},
		{
			type = "item",
			name = "flour",
			amount = 1
		}
	},
	results = {
		{
			type = "item",
			name = "dough",
			amount = 1
		}
	},
	icon = "__pizzatorio__/graphics/icons/dough.png",
	icon_size = 32,
	energy = 100,
	group = "intermediate-products",
	subgroup = "other"
}

data:extend{dough_recipe}
