local mushroom_pizza_recipe = {
	type = "recipe",
	name = "mushroom-pizza",
	enabled = true,
	hidden = false,
	category = "smelting",
	ingredients = {
		{
			type = "item",
			name = "raw-mushroom-pizza",
			amount = 1
		}
	},
	results = {
		{
			type = "item",
			name = "mushroom-pizza",
			amount = 1
		}
	},
	icon = "__pizzatorio__/graphics/icons/mushroom-pizza.png",
	icon_size = 32,
	energy = 10,
	group = "intermediate-products",
	subgroup = "other"
}

data:extend{mushroom_pizza_recipe}
