local mushroom_recipe = {
	type = "recipe",
	name = "mushroom",
	enabled = true,
	hidden = false,
	category = "crafting-with-fluid",
	ingredients = {
		{
			type = "fluid",
			name = "water",
			amount = 10
		}
	},
	results = {
		{
			type = "item",
			name = "mushroom",
			amount = 1
		}
	},
	icon = "__pizzatorio__/graphics/icons/mushroom.png",
	icon_size = 32,
	energy = 100,
	group = "intermediate-products",
	subgroup = "other"
}

data:extend{mushroom_recipe}
