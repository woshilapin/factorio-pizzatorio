local meal_pack_recipe = {
	type = "recipe",
	name = "meal-pack",
	enabled = true,
	hidden = false,
	category = "crafting",
	ingredients = {
		{
			type = "item",
			name = "cheese-pizza",
			amount = 5
		},{
			type = "item",
			name = "mushroom-pizza",
			amount = 5
		}
	},
	results = {
		{type="item", name="meal-pack", amount=1}
	},
	icon = "__base__/graphics/entity/iron-chest/iron-chest.png",
	icon_size = 32,
	energy = 100,
	group = "intermediate-products",
	subgroup = "other",
	order = "b"
}

data:extend{ meal_pack_recipe }
