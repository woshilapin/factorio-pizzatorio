local fisheroni_recipe = {
	type = "recipe",
	name = "fisheroni",
	enabled = true,
	hidden = false,
	category = "smelting",
	ingredients = {
		{
			type = "item",
			name = "raw-fish",
			amount = 1
		}
	},
	results = {
		{
			type = "item",
			name = "fisheroni",
			amount = 1
		}
	},
	icon = "__pizzatorio__/graphics/icons/fisheroni.png",
	icon_size = 32,
	energy = 10,
	group = "intermediate-products",
	subgroup = "other"
}

data:extend{fisheroni_recipe}
