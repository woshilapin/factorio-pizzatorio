local yeast_recipe = {
	type = "recipe",
	name = "yeast",
	enabled = true,
	hidden = false,
	category = "crafting",
	ingredients = {
		{
			type = "item",
			name = "mushroom",
			amount = 10
		}
	},
	results = {
		{
			type = "item",
			name = "yeast",
			amount = 1
		}
	},
	icon = "__pizzatorio__/graphics/icons/yeast.png",
	icon_size = 32,
	energy = 100,
	group = "intermediate-products",
	subgroup = "other"
}

data:extend{yeast_recipe}
