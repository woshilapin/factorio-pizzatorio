local milk_recipe = {
	type = "recipe",
	name = "milk",
	enabled = true,
	hidden = false,
	category = "oil-processing",
	ingredients = {
		{
			type = "item",
			name = "raw-fish",
			amount = 10
		}
	},
	results = {
		{
			type = "fluid",
			name = "milk",
			amount = 1
		}
	},
	icon = "__pizzatorio__/graphics/icons/milk.png",
	icon_size = 32,
	energy = 10,
	group = "intermediate-products",
	subgroup = "other"
}

data:extend{milk_recipe}
