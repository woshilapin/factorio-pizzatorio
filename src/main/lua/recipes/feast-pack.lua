local feast_pack_recipe = {
	type = "recipe",
	name = "feast-pack",
	enabled = true,
	hidden = false,
	category = "crafting",
	ingredients = {
		{
			type = "item",
			name = "cheese-pizza",
			amount = 20
		},
		{
			type = "item",
			name = "mushroom-pizza",
			amount = 15
		},{
			type = "item",
			name = "fisheroni-pizza",
			amount = 15
		}
	},
	results = {
		{type="item", name="feast-pack", amount=1}
	},
	icon = "__base__/graphics/entity/steel-chest/steel-chest.png",
	icon_size = 32,
	energy = 100,
	group = "intermediate-products",
	subgroup = "other",
	order = "c"
}

data:extend{ feast_pack_recipe }
