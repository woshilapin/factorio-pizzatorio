local cheese_pizza_recipe = {
	type = "recipe",
	name = "cheese-pizza",
	enabled = true,
	hidden = false,
	category = "smelting",
	ingredients = {
		{
			type = "item",
			name = "raw-cheese-pizza",
			amount = 1
		}
	},
	results = {
		{
			type = "item",
			name = "cheese-pizza",
			amount = 1
		}
	},
	icon = "__pizzatorio__/graphics/icons/cheese-pizza.png",
	icon_size = 32,
	energy = 10,
	group = "intermediate-products",
	subgroup = "other"
}

data:extend{cheese_pizza_recipe}
