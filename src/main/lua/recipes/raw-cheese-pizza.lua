local raw_cheese_pizza_recipe = {
	type = "recipe",
	name = "raw-cheese-pizza",
	enabled = true,
	hidden = false,
	category = "crafting-with-fluid",
	ingredients = {
		{
			type = "item",
			name = "dough",
			amount = 1
		},
		{
			type = "item",
			name = "cheese",
			amount = 1
		},
		{
			type = "item",
			name = "tomato",
			amount = 3
		},
	},
	results = {
		{
			type = "item",
			name = "raw-cheese-pizza",
			amount = 1
		}
	},
	icon = "__pizzatorio__/graphics/icons/raw-cheese-pizza.png",
	icon_size = 32,
	energy = 100,
	group = "intermediate-products",
	subgroup = "other"
}

data:extend{raw_cheese_pizza_recipe}
