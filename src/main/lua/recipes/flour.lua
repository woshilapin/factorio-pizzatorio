local flour_recipe = {
	type = "recipe",
	name = "flour",
	enabled = true,
	hidden = false,
	category = "crafting",
	ingredients = {
		{
			type = "item",
			name = "wheat",
			amount = 10
		}
	},
	results = {
		{
			type = "item",
			name = "flour",
			amount = 1
		}
	},
	icon = "__pizzatorio__/graphics/icons/flour.png",
	icon_size = 32,
	energy = 100,
	group = "intermediate-products",
	subgroup = "other"
}

data:extend{flour_recipe}
