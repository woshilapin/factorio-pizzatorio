local greenhouse_recipe = {
	type = "recipe",
	name = "greenhouse",
	enabled = true,
	hidden = false,
	category = "crafting",
	ingredients = {
		{
			type = "item",
			name = "wood",
			amount = 10
		},
		{
			type = "item",
			name = "iron-plate",
			amount = 4
		}
	},
	results = {
		{
			type = "item",
			name = "greenhouse",
			amount = 1
		}
	},
	icons = {
		{
			icon = "__base__/graphics/icons/lab.png",
			tint = { r = 0.34, g = 0.72, b = 0.28, a = 0.6 }
		}
	},
	icon_size = 32,
	energy = 100,
	group = "intermediate-products",
	subgroup = "other"
}

data:extend{greenhouse_recipe}
