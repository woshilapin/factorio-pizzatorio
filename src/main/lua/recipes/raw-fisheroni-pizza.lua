local raw_fisheroni_pizza_recipe = {
	type = "recipe",
	name = "raw-fisheroni-pizza",
	enabled = true,
	hidden = false,
	category = "crafting-with-fluid",
	ingredients = {
		{
			type = "item",
			name = "dough",
			amount = 1
		},
		{
			type = "item",
			name = "cheese",
			amount = 1
		},
		{
			type = "item",
			name = "tomato",
			amount = 3
		},
		{
			type = "item",
			name = "mushroom",
			amount = 2
		},
		{
			type = "item",
			name = "fisheroni",
			amount = 5
		}
	},
	results = {
		{
			type = "item",
			name = "raw-fisheroni-pizza",
			amount = 1
		}
	},
	icon = "__pizzatorio__/graphics/icons/raw-fisheroni-pizza.png",
	icon_size = 32,
	energy = 100,
	group = "intermediate-products",
	subgroup = "other"
}

data:extend{raw_fisheroni_pizza_recipe}
