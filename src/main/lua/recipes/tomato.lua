local tomato_recipe = {
	type = "recipe",
	name = "tomato",
	enabled = true,
	hidden = false,
	category = "crafting-with-fluid",
	ingredients = {
		{
			type = "fluid",
			name = "water",
			amount = 10
		}
	},
	results = {
		{
			type = "item",
			name = "tomato",
			amount = 1
		}
	},
	icon = "__pizzatorio__/graphics/icons/tomato.png",
	icon_size = 32,
	energy = 100,
	group = "intermediate-products",
	subgroup = "other"
}

data:extend{tomato_recipe}
