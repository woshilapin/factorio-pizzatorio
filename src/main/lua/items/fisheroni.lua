local fisheroni = {
	type = "item",
	name = "fisheroni",
	icon = "__pizzatorio__/graphics/icons/fisheroni.png",
	icon_size = 32,
	stack_size = 200
}

data:extend{fisheroni}
