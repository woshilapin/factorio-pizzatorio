local fisheroni_pizza = {
	type = "item",
	name = "fisheroni-pizza",
	icon = "__pizzatorio__/graphics/icons/fisheroni-pizza.png",
	icon_size = 32,
	stack_size = 50
}

data:extend{fisheroni_pizza}
