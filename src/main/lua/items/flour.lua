local flour = {
	type = "item",
	name = "flour",
	icon = "__pizzatorio__/graphics/icons/flour.png",
	icon_size = 32,
	stack_size = 50
}

data:extend{flour}
