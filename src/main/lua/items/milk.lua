local milk = {
	type = "fluid",
	name = "milk",
	default_temperature = 25,
	heat_capacity = "0.1KJ",
	base_color = {r=1, g=1, b=1},
	flow_color = {r=0.5, g=0.5, b=0.5},
	max_temperature = 100,
	icon = "__pizzatorio__/graphics/icons/milk.png",
	icon_size = 32,
	pressure_to_speed_ratio = 0.4,
	flow_to_energy_ratio = 0.59,
	-- fuel_value = "8MJ",
	order = "a[fluid]-b[crude-oil]"
}

data:extend{milk}
