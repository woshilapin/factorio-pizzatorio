local raw_mushroom_pizza = {
	type = "item",
	name = "raw-mushroom-pizza",
	icon = "__pizzatorio__/graphics/icons/raw-mushroom-pizza.png",
	icon_size = 32,
	stack_size = 50
}

data:extend{raw_mushroom_pizza}
