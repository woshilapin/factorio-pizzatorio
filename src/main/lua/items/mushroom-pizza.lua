local mushroom_pizza = {
	type = "item",
	name = "mushroom-pizza",
	icon = "__pizzatorio__/graphics/icons/mushroom-pizza.png",
	icon_size = 32,
	stack_size = 50
}

data:extend{mushroom_pizza}
