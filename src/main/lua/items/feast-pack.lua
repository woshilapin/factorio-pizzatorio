local feast_pack = {
	type = "item",
	name = "feast-pack",
	icon = "__base__/graphics/entity/steel-chest/steel-chest.png",
	icon_size = 32,
	stack_size = 50
}

data:extend{ feast_pack }
