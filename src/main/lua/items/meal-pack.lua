local meal_pack = {
	type = "item",
	name = "meal-pack",
	icon = "__base__/graphics/entity/iron-chest/iron-chest.png",
	icon_size = 32,
	stack_size = 50
}

data:extend{ meal_pack }
