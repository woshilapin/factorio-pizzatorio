local wheat = {
	type = "item",
	name = "wheat",
	icon = "__pizzatorio__/graphics/icons/wheat.png",
	icon_size = 32,
	stack_size = 50
}

data:extend{wheat}
