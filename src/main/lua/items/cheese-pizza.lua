local cheese_pizza = {
	type = "item",
	name = "cheese-pizza",
	icon = "__pizzatorio__/graphics/icons/cheese-pizza.png",
	icon_size = 32,
	stack_size = 50
}

data:extend{cheese_pizza}
