local dough = {
	type = "item",
	name = "dough",
	icon = "__pizzatorio__/graphics/icons/dough.png",
	icon_size = 32,
	stack_size = 50
}

data:extend{dough}
