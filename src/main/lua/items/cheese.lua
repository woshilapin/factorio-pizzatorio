local cheese = {
	type = "item",
	name = "cheese",
	icon = "__pizzatorio__/graphics/icons/cheese.png",
	icon_size = 32,
	stack_size = 50
}

data:extend{cheese}
