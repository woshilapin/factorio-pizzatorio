[![build](https://gitlab.com/pizzatorio/factorio-pizzatorio/badges/master/build.svg)](https://gitlab.com/pizzatorio/factorio-pizzatorio/pipelines)
[![coverage](https://codecov.io/gl/pizzatorio/factorio-pizzatorio/branch/master/graph/badge.svg)](https://codecov.io/gl/pizzatorio/factorio-pizzatorio)

# Pizzatorio [Factorio Mod]
This mod will allow you to create pizzas, like the so famous *fisheroni pizza*.

## Install
Working with Luarocks is going to simplify your life.  On Mac OS X, you can
install it with Homebrew.

```
brew install luarocks
```

Then you can install the dependencies needed.

```
luarocks install busted cluacov luacheck
```

## Tests
To run the test suite, just launch the following command.

```
busted
```

## Quality
Luacheck is the tool we use for checking the quality of the project. To get an
analysis, run the following command.

```
luacheck .
```
